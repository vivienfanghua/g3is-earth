var SETTING = {
    AccessToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZDljNWU5MS1lNWZhLTQwZTctOThlNS0wNThhMTFjNGQ2NWQiLCJpZCI6MjI4MCwiaWF0IjoxNTMyNDMzNzI1fQ.oIJZWzAcX_Hl1xPppq1s6CYCFrWA8481Nhkov64DxUY'

};

var LON_LAT_TABLE = {
    0: 512,
    1: 256,
    2: 128,
    3: 64,
    4: 32,
    5: 16,
    6: 8,
    7: 4,
    8: 2,
    9: 1,
    10: 1 / 2,
    11: 1 / 4,
    12: 1 / 8,
    13: 1 / 16,
    14: 1 / 32,
    15: 1 / 64,
    16: 1 / 128,
    17: 1 / 256,
    18: 1 / 512,
    19: 1 / 1024,
    20: 1 / 2048,
    21: 1 / 5096
};
var HEIGHT_TABLE ={

};

function create_table(max_height) {
    HEIGHT_TABLE[0] = max_height;
    for(var i = 1; i < 32; i++){
        HEIGHT_TABLE[i] = HEIGHT_TABLE[i-1]/2;
    }
}

create_table(Math.pow(2,25));
